//
//  GBPointsTableViewCell.swift
//  GBKSOFT
//
//  Created by admin on 6/13/19.
//  Copyright © 2019 Andrew Sasin. All rights reserved.
//

import UIKit

class GBPointsTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    let namePoint : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.font = UIFont.boldSystemFont(ofSize: 15)
        lbl.textAlignment = .left
        lbl.numberOfLines = 0
        return lbl
    }()
    
    let latitudePoint : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.font = UIFont.boldSystemFont(ofSize: 13)
        lbl.textAlignment = .left
        return lbl
    }()
    
    let longitudePoint : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.font = UIFont.boldSystemFont(ofSize: 13)
        lbl.textAlignment = .left
        return lbl
    }()
    
    let mainView: UIView = {
        let view = UIView()
        view.layer.borderColor = UIColor.black.cgColor
        view.layer.borderWidth = 1.0
        return view
    }()
    
    let rectTriangle: UIView = {
        let view = GBRectTriangel()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(mainView)
        
        mainView.addSubview(namePoint)
        mainView.addSubview(latitudePoint)
        mainView.addSubview(longitudePoint)
        mainView.addSubview(rectTriangle)
    
        rectTriangle.snp.makeConstraints { (make) -> Void in
            make.centerY.equalTo(mainView)
            make.height.equalTo(50)
            make.width.equalTo(100)
            make.trailing.equalTo(mainView).offset(-17)
        }
        
        mainView.snp.makeConstraints { (make) -> Void in
            make.topMargin.equalTo(self).offset(17)
            make.leadingMargin.equalTo(self).offset(17)
            make.rightMargin.equalTo(self).offset(-17)
            make.bottomMargin.equalTo(self).offset(-17)
        }
        
        namePoint.snp.makeConstraints { (make) -> Void in
            make.topMargin.equalTo(mainView).offset(17)
            make.leadingMargin.equalTo(mainView).offset(17)
            make.right.equalTo(rectTriangle.snp_leftMargin).offset(-30)
        }
        
        latitudePoint.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(namePoint.snp_bottomMargin).offset(17)
            make.leftMargin.equalTo(mainView).offset(17)
            make.right.equalTo(rectTriangle.snp_leftMargin).offset(-30)
        }

        longitudePoint.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(latitudePoint.snp_bottomMargin).offset(17)
            make.leftMargin.equalTo(mainView).offset(17)
            make.right.equalTo(rectTriangle.snp_leftMargin).offset(-30)
            make.bottomMargin.equalTo(mainView).offset(-17)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func createCell(coordinate: coordinate){
//        let arrayKeys = Array(dictCoordinate.keys)
//
//        guard let coordinate = dictCoordinate[arrayKeys[index.row]] else{
//            return
//        }

        namePoint.text = coordinate.name
        latitudePoint.text = "\(coordinate.latitude ?? 0.0)"
        longitudePoint.text = "\(coordinate.longitude ?? 0.0)"
    }
    
}
