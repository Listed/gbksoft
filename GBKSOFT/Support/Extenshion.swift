//
//  Extenshion.swift
//  GBKSOFT
//
//  Created by admin on 6/13/19.
//  Copyright © 2019 Andrew Sasin. All rights reserved.
//

import Foundation

extension Date {
    var asUUID: String {
        let asInteger = Int(self.timeIntervalSince1970)
        return String(asInteger)
    }
}

extension Notification.Name {
    static let NotificationUpdateDB = Notification.Name("NotificationUpdateDB")
    static let NotificationChildRemove = Notification.Name("NotificationChildRemove")
}
