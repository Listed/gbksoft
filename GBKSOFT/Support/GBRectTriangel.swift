//
//  GBRectTriangel.swift
//  GBKSOFT
//
//  Created by admin on 6/13/19.
//  Copyright © 2019 Andrew Sasin. All rights reserved.
//

import UIKit

class GBRectTriangel : UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func draw(_ rect: CGRect) {
        
        guard let context = UIGraphicsGetCurrentContext() else { return }
        
        context.beginPath()
        context.move(to: CGPoint(x: rect.minX, y: rect.minY))
        context.addLine(to: CGPoint(x: rect.maxX*0.7, y: rect.minY))
        context.addLine(to: CGPoint(x: (rect.maxX), y: rect.maxY / 2.0))
        context.addLine(to: CGPoint(x: rect.maxX*0.7, y: rect.maxY))
        context.addLine(to: CGPoint(x: rect.minX, y: rect.maxY))
        context.closePath()
        
        context.setFillColor(red: 1.0, green: 0.5, blue: 0.0, alpha: 0.60)
        context.fillPath()
    }
}
