//
//  ViewController.swift
//  GBKSOFT
//
//  Created by admin on 6/12/19.
//  Copyright © 2019 Andrew Sasin. All rights reserved.
//

import UIKit
import SnapKit
import GoogleSignIn

enum AnimationType{
    case ANIMATE_RIGHT
    case ANIMATE_LEFT
    case ANIMATE_UP
    case ANIMATE_DOWN
}

class GBLoginViewController: UIViewController,GIDSignInUIDelegate {

    lazy var googleAuthButton : UIButton = {
        let button = UIButton()
        button.setTitle("Auth", for: .normal)
        button.backgroundColor = UIColor.red
        button.layer.cornerRadius = 50
        button.addTarget(self, action: #selector(actioAuth), for: .touchUpInside)
        return button
    }()
    
    //MARK: - Life Cycel
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        createUI()
    }

    deinit {
        print("deinit")
    }
    //MARK: Private methods
    
    private func createUI(){
        self.title = "GBKSoftTest"
        self.view.addSubview(googleAuthButton)
        
        googleAuthButton.snp.makeConstraints { (make) -> Void in
            make.width.equalTo(100)
            make.height.equalTo(100)
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        }
    }
    
    private func showViewControllerWith(newViewController:UIViewController, usingAnimation animationType:AnimationType)
    {
        
        let currentViewController = UIApplication.shared.delegate?.window??.rootViewController
        let width = currentViewController?.view.frame.size.width;
        let height = currentViewController?.view.frame.size.height;
        
        var previousFrame:CGRect?
        var nextFrame:CGRect?
        
        switch animationType
        {
        case .ANIMATE_LEFT:
            previousFrame = CGRect.init(x: width!-1, y: 0.0, width: width!, height: height!)
            nextFrame = CGRect.init(x: -width!, y: 0.0, width: width!, height: height!)
        case .ANIMATE_RIGHT:
            previousFrame = CGRect.init(x: -width!+1, y: 0.0, width: width!, height: height!)
            nextFrame = CGRect.init(x: width!, y: 0.0, width: width!, height: height!)
        case .ANIMATE_UP:
            previousFrame = CGRect.init(x: 0.0, y: height!-1, width: width!, height: height!)
            nextFrame = CGRect.init(x: 0.0, y: -height!+1, width: width!, height: height!)
        case .ANIMATE_DOWN:
            previousFrame = CGRect.init(x: 0.0, y: -height!+1, width: width!, height: height!)
            nextFrame = CGRect.init(x: 0.0, y: 0.0, width: height!-1, height: height!)
        }
        
        newViewController.view.frame = previousFrame!
        UIApplication.shared.delegate?.window??.addSubview(newViewController.view)
        UIView.animate(withDuration: 0.33,
                       animations: { () -> Void in
                        newViewController.view.frame = (currentViewController?.view.frame)!
                        currentViewController?.view.frame = nextFrame!
                        
        })
        { (fihish:Bool) -> Void in
            UIApplication.shared.delegate?.window??.rootViewController = newViewController
        }
    }
    //MARK: Actions
    @objc func actioAuth(sender: UIButton!) {
        GIDSignIn.sharedInstance().signIn()
    }
    
}

//MARK:- Google Delegate
extension GBLoginViewController: GIDSignInDelegate{
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        print("Error")
    }
    
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    public func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
                     withError error: Error!) {
        if (error == nil) {
           // self.navigationController?.pushViewController(UINavigationController(rootViewController: GBTabBarViewController()), animated: true)
           showViewControllerWith(newViewController: UINavigationController(rootViewController: GBTabBarViewController()), usingAnimation: AnimationType.ANIMATE_LEFT)
            //TODO:Пример если нужны данные пользователя
//            // Perform any operations on signed in user here.
//            let userId = user.userID                  // For client-side use only!
//            let idToken = user.authentication.idToken // Safe to send to the server
//            let fullName = user.profile.name
//            let givenName = user.profile.givenName
//            let familyName = user.profile.familyName
//            let email = user.profile.email
        } else {
            print("\(String(describing: error))")
        }
    }
}

