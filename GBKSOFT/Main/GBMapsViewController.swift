//
//  GBMapsViewController.swift
//  GBKSOFT
//
//  Created by admin on 6/12/19.
//  Copyright © 2019 Andrew Sasin. All rights reserved.
//

import UIKit
import GoogleMaps

class GBMapsViewController: UIViewController,GMUClusterRendererDelegate {

    var locationManager = CLLocationManager()
    var pointDB : [(key: String, value: coordinate)]?
    var itemsCluster: [GBClasterModel] = []
    var markers = [GMSMarker]()
    var setMarkersCluster: Set = Set<GMSMarker>()
    
    var selectedPoints: (key: String, value: coordinate)?{
        didSet {
            let index = markers.firstIndex(where: {$0.title == selectedPoints!.value.name})
            if index != nil{
                mapView.selectedMarker = markers[index!]
                mapView.camera = GMSCameraPosition.camera(withTarget: markers[index!].position, zoom: 9.0)
            }
        }
    }
    
    private var clusterManager: GMUClusterManager!
    
    private let mapView :GMSMapView = {
        let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 6.0)
        let map = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        map.isMyLocationEnabled = true
        return map
    }()
    
    //MARK: Life cycle
    override func loadView() {
        view = mapView
    }
    
    //Mark: LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Location Manager code to fetch current location
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        
        NotificationCenter.default.addObserver(self, selector: #selector(updatedPoint(notification:)), name: .NotificationUpdateDB, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(childRemove(notification:)), name: .NotificationChildRemove, object: nil)
        
        let iconGenerator = GMUDefaultClusterIconGenerator()
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: mapView,
                                                 clusterIconGenerator: iconGenerator)
        renderer.delegate = self
        clusterManager = GMUClusterManager(map: mapView, algorithm: algorithm,
                                           renderer: renderer)
        clusterManager.setDelegate(self, mapDelegate: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.navigationItem.title = "Maps"
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        print("Deinit Maps")
    }
    
    //MARK: Private func
    func createMarkers(){
        guard let point = self.pointDB else{
            return
        }
        
        for coordinateDescript in point {
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: coordinateDescript.value.latitude ?? 0.0, longitude: coordinateDescript.value.longitude ?? 0.0)
            marker.icon = UIImage(named: "pin_locations")
            marker.title = coordinateDescript.value.name
            
            let result = markers.contains { $0.title == marker.title }
            
            if !result{
                marker.map = mapView
                markers.append(marker)
            }
            
        }
    }
    
    func createCluster(){
        
        generateClusterItems()
        
        clusterManager.cluster()
    }
    
    private func generateClusterItems(){
        guard let point = self.pointDB else{
            return
        }
        
        for coordinateDescript in point {
            let name = coordinateDescript.value.name ?? "Not found name"
            
            guard let latide = coordinateDescript.value.latitude, let longtude =  coordinateDescript.value.longitude else{
                return
            }
            
            let item =
                GBClasterModel(position: CLLocationCoordinate2DMake(latide, longtude), name: name)
            
            let result = itemsCluster.contains { $0.name == item.name }
            
            //TODO: нужно проверять на изменение данных или добавить с Firebase observer на изменение данных
            if !result{
                itemsCluster.append(item)
                clusterManager.add(item)
            }
        }
    }
}

extension GBMapsViewController:CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        
        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 17.0)
        
        self.mapView.animate(to: camera)
        
        //Finally stop updating location otherwise it will come again and again in this delegate
        self.locationManager.stopUpdatingLocation()
    }
}

extension GBMapsViewController{
    @objc func updatedPoint(notification: NSNotification) {
        let pointGet = notification.object as? [(key: String, value: coordinate)]
        
        guard let point = pointGet else{
            return
        }
    
        self.pointDB = point
        
        if point.count > 2{
            markers.removeAll()
            mapView.clear()
            createCluster()
        }else{
            if itemsCluster.count > 0{
                for iteam in itemsCluster{
                    clusterManager.remove(iteam)
                }
                itemsCluster.removeAll()
                clusterManager.cluster()
            }

            createMarkers()
        }
    }
    
    @objc func childRemove(notification: NSNotification) {
        let pointGet = notification.object as? String
        
        guard pointGet != nil else{
            return
        }
        
        
        if itemsCluster.count > 0{
            let index = itemsCluster.firstIndex(where: {$0.name == pointGet})
            if index != nil{
                clusterManager.remove(itemsCluster[index!])
                itemsCluster.remove(at: index!)
                clusterManager.cluster()
            }
        }else{
            
            let index = markers.firstIndex(where: {$0.title == pointGet})
            if index != nil{
                markers[index!].map = nil
                markers.remove(at: index!)
            }
        }
    }
    
}

extension GBMapsViewController: GMUClusterManagerDelegate,GMSMapViewDelegate{
    func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) -> Bool {
        let newCamera = GMSCameraPosition.camera(withTarget: cluster.position,
                                                 zoom: mapView.camera.zoom)
        let update = GMSCameraUpdate.setCamera(newCamera)
        mapView.moveCamera(update)
        return true
    }

    func clusterManager(_ clusterManager: GMUClusterManager, didTap clusterItem: GMUClusterItem) -> Bool {
        return false
    }

    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
       
        if let poiItem = marker.userData as? GBClasterModel {
            marker.title = poiItem.name
            NSLog("Did tap marker for cluster item \(poiItem.name ?? "Unknown")")
        } else {
            NSLog("Did tap a normal marker")
        }

        mapView.selectedMarker = marker
        mapView.camera = GMSCameraPosition.camera(withTarget: marker.position, zoom: 9.0)
        return true
    }
    
    func renderer(_ renderer: GMUClusterRenderer, willRenderMarker marker: GMSMarker) {
        setMarkersCluster.insert(marker)
    }
    
    func renderer(_ renderer: GMUClusterRenderer, markerFor object: Any) -> GMSMarker? {
        let marker = GMSMarker()
        if object is GBClasterModel {
            marker.icon = UIImage(named: "pin_cluster")
        }
        return marker
    }
    
    func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        let alert = UIAlertController(title: "\(coordinate.latitude)", message: "\(coordinate.longitude)", preferredStyle: .alert)
        
        alert.addTextField { (textField) in
            textField.text = ""
        }
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0]
            guard let text = textField?.text else{
                return
            }
            GPFirebaseManager.shared.addChilds(name: text, latide: coordinate.latitude, longtude: coordinate.longitude)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
}
