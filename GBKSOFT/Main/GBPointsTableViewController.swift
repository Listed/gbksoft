//
//  GBPointsTableViewController.swift
//  GBKSOFT
//
//  Created by admin on 6/12/19.
//  Copyright © 2019 Andrew Sasin. All rights reserved.
//

import UIKit

class GBPointsTableViewController: UIViewController,UIGestureRecognizerDelegate {
    
    let cellIdentifier = "AudioCell"
    
    var point: [(key: String, value: coordinate)]?
    
    let emptyLabel : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.font = UIFont.boldSystemFont(ofSize: 20)
        lbl.textAlignment = .center
        lbl.numberOfLines = 0
        lbl.text = "Пока нет ни одной точки"
        return lbl
    }()
    
    let tableView: UITableView = {
        let table = UITableView()
        return table
    }()
    
    //MARK:Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createUI()
        
        NotificationCenter.default.addObserver(self, selector: #selector(updatedPoint(notification:)), name: .NotificationUpdateDB, object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.navigationItem.title = "List"
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        print("Deinit Points")
    }
    
    //MARK: Private func
    private func needSeeLabel(isNeedLabel: Bool) -> (Void){
        if isNeedLabel{
            self.tableView.isHidden = true
            self.emptyLabel.isHidden = false
        }else{
            self.tableView.isHidden = false
            self.emptyLabel.isHidden = true
        }
    }
    
    private func createUI(){
        self.view.addSubview(tableView)
        self.view.addSubview(emptyLabel)
        
        self.view.backgroundColor = UIColor.white
        
        tableView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        emptyLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.view)
            //make.centerX.equalTo(self.view)
            make.trailingMargin.equalTo(self.view).offset(-17)
            make.leadingMargin.equalTo(self.view).offset(17)
        }
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.isHidden = true
        tableView.estimatedRowHeight = 300
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(GBPointsTableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress))
        longPressGesture.minimumPressDuration = 1.0
        longPressGesture.delegate = self
        tableView.addGestureRecognizer(longPressGesture)
    }
    
    //MARK: Actions
    @objc func handleLongPress(_ gestureRecognizer: UILongPressGestureRecognizer){
        if gestureRecognizer.state == .ended {
            let touchPoint = gestureRecognizer.location(in: tableView)
            if let indexPath = tableView.indexPathForRow(at: touchPoint) {
                guard let point = self.point else {
                    return
                }
                let keysRemove = point[indexPath.row]
                
                GPFirebaseManager.shared.remove(keysChild: keysRemove.key)
            }
        }
    }
}


extension GBPointsTableViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let point = self.point else {
            return 0
        }
        
        return point.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : GBPointsTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)! as! GBPointsTableViewCell
        
        guard let point = self.point else {
            return cell
        }
        
        let coordinate = point[indexPath.row]
        
        cell.createCell(coordinate: coordinate.value)
        
        return cell
    }
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        
        guard let point = self.point else {
            return
        }
        
        let coordinate = point[indexPath.row]
        (tabBarController!.viewControllers![1] as! GBMapsViewController).selectedPoints = coordinate
        
        tabBarController?.selectedIndex = 1
    }
}


extension GBPointsTableViewController{
    @objc func updatedPoint(notification: NSNotification) {
        let pointGet = notification.object as? [(key: String, value: coordinate)]
        
        guard pointGet != nil else{
            self.needSeeLabel(isNeedLabel: true)
            self.tableView.reloadData()
            return
        }

        self.point = pointGet
        
        self.needSeeLabel(isNeedLabel: false)
        
        self.tableView.reloadData()
    }
}
