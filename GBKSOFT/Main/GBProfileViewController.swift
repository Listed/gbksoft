//
//  GBProfileViewController.swift
//  GBKSOFT
//
//  Created by admin on 6/14/19.
//  Copyright © 2019 Andrew Sasin. All rights reserved.
//

import UIKit
import GoogleSignIn

class GBProfileViewController: UIViewController {

    lazy var googleLogOutButton : UIButton = {
        let button = UIButton()
        button.setTitle("LogOut", for: .normal)
        button.backgroundColor = UIColor.red
        button.layer.cornerRadius = 50
        button.addTarget(self, action: #selector(actioLogOut), for: .touchUpInside)
        return button
    }()
    
    //MARK: Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.navigationItem.title = "Profile"
    }
    
    //MARK: Private methods
    
    private func createUI(){
        self.view.addSubview(googleLogOutButton)
        
        googleLogOutButton.snp.makeConstraints { (make) -> Void in
            make.width.equalTo(100)
            make.height.equalTo(100)
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        }
    }
    
    //MARK: Actions
    @objc func actioLogOut(sender: UIButton!) {
        GIDSignIn.sharedInstance().signOut()
        GPFirebaseManager.shared.removeRefObserver()
        
        GPFirebaseManager.shared.isFromLogOut = true
        let nav1 = UINavigationController()
        let mainView = GBLoginViewController(nibName: nil, bundle: nil)
        mainView.view.backgroundColor = UIColor.white
        nav1.viewControllers = [mainView]
        UIApplication.shared.keyWindow?.rootViewController = nav1
    }
}
