//
//  GBTabBarViewController.swift
//  GBKSOFT
//
//  Created by admin on 6/12/19.
//  Copyright © 2019 Andrew Sasin. All rights reserved.
//

import UIKit

class GBTabBarViewController: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let firstViewController = GBPointsTableViewController()
        firstViewController.tabBarItem = UITabBarItem(title: "List", image: nil, tag: 0)
        
        let secondViewController = GBMapsViewController()
        secondViewController.view.backgroundColor = UIColor.white
        secondViewController.tabBarItem = UITabBarItem(title: "Maps", image: nil, tag: 1)
        
        let profileViewController = GBProfileViewController()
        profileViewController.view.backgroundColor = UIColor.white
        
        profileViewController.tabBarItem = UITabBarItem(title: "Profile", image: nil, tag: 2)
        
        let tabBarList = [firstViewController, secondViewController,profileViewController]
        
        viewControllers = tabBarList
        
        if GPFirebaseManager.shared.isFromLogOut{
            GPFirebaseManager.shared.oneUpdate()
            GPFirebaseManager.shared.isFromLogOut = false
        }
    }

}
