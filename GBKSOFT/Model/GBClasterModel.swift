//
//  GBClasterModel.swift
//  GBKSOFT
//
//  Created by admin on 6/13/19.
//  Copyright © 2019 Andrew Sasin. All rights reserved.
//

import Foundation
import GoogleMaps

class GBClasterModel: NSObject, GMUClusterItem {
    var position: CLLocationCoordinate2D
    var name: String!
    
    init(position: CLLocationCoordinate2D, name: String) {
        self.position = position
        self.name = name
    }
}
