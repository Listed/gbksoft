//
//  GPFirebaseManager.swift
//  GBKSOFT
//
//  Created by admin on 6/13/19.
//  Copyright © 2019 Andrew Sasin. All rights reserved.
//

import Foundation
import Firebase

class GPFirebaseManager{
    
    static let shared = GPFirebaseManager()
    
    var ref = Database.database().reference()
    var point: [(key: String, value: coordinate)]?
    
    var isFromLogOut = false
    //Initializer access level change now
    private init(){
//        ref.child("names").setValue([Date().asUUID: ["name":"sd fad fasd fasdfg hkjasgd fhjgasfhadhksfas dfhjkags dfhjag sjdhfg ashjf ashjdfg hajsdg fhjkadgs fhjkadgsfhjkags fhj sasdfasfdasdfsasad fasd fasd f asdf asdf asd fas df ", "latitude":50.231535125,"longitude":52.231535125]])
//        
//        ref.child("names").child("333").setValue(["name":"zz fad fasd fasdfg hkjasgd fhjgasfhadhksfas dfhjkags dfhjag sjdhfg ashjf ashjdfg hajsdg fhjkadgs fhjkadgsfhjkags fhj sasdfasfdasdfsasad fasd fasd f asdf asdf asd fas df ", "latitude":51.231535125,"longitude":52.231535125])
//        
//        ref.child("names").child("222").setValue(["name":"aaa fad fasd fasdfg hkjasgd fhjgasfhadhksfas dfhjkags dfhjag sjdhfg ashjf ashjdfg hajsdg fhjkadgs fhjkadgsfhjkags fhj sasdfasfdasdfsasad fasd fasd f asdf asdf asd fas df ", "latitude":50.231535125,"longitude":51.231535125])
//        
//        ref.child("names").child("2223").setValue(["name":"111 fad fasd fasdfg hkjasgd fhjgasfhadhksfas dfhjkags dfhjag sjdhfg ashjf ashjdfg hajsdg fhjkadgs fhjkadgsfhjkags fhj sasdfasfdasdfsasad fasd fasd f asdf asdf asd fas df ", "latitude":51.121535125,"longitude":51.141535125])
        //TODO: Для рассмотрения
        //В зависимости от задачи, что в дальнейшем надо использовать
        //Можно сделать observer на разные действия: Удаление, Изменение, Добавление, Редактирование, Все вместе
        //В данном случае observer на все изменения, так же для Firebase console (.value)
        //Можно было сделать activity при получении данных один раз и подключить на Удаление и Добавления, чтобы не делать перезагрузку таблицы постоянно,а использовать append(remove index с массива), но не знаю ,что Вы хотите делать с FireBaseConsole
        //for sorted child("names").queryOrdered( byChild: "name")
        ref.observe(.value, with: { [weak self](snapshot) in
            
            guard let self = self else {
                return
            }
            
            let postDict = snapshot.value as? [String : AnyObject] ?? [:]
            
            do {
                let pointGet = try GBPoints(dictionary: postDict)
                
                guard pointGet.names != nil else {
                    NotificationCenter.default.post(name: .NotificationUpdateDB, object: nil)
                    return
                }
                
                self.point = pointGet.names!.sorted(by:{ $0.1.name! < $1.1.name! })
                
                NotificationCenter.default.post(name: .NotificationUpdateDB, object: self.point)
            } catch {
                NotificationCenter.default.post(name: .NotificationUpdateDB, object: nil)
            }
        })
        
        ref.child("names").observe(.childRemoved, with: {(snapshot) in
            let postDict = snapshot.value as? [String : AnyObject] ?? [:]
            guard let names = postDict["name"] else{
                return
            }
            
            NotificationCenter.default.post(name: .NotificationChildRemove, object: names)
          
        })
    }

    
    deinit {
       print("De init")
    }
    
    func removeRefObserver(){
        ref.removeAllObservers()
    }
    
    func oneUpdate() {
        ref.observeSingleEvent(of: .value, with: {[weak self] (snapshot) in
            guard let self = self else {
                return
            }
            
            let postDict = snapshot.value as? [String : AnyObject] ?? [:]
            
            do {
                let pointGet = try GBPoints(dictionary: postDict)
                
                guard pointGet.names != nil else {
                    NotificationCenter.default.post(name: .NotificationUpdateDB, object: nil)
                    return
                }
                
                self.point = pointGet.names!.sorted(by:{ $0.1.name! < $1.1.name! })
                
                NotificationCenter.default.post(name: .NotificationUpdateDB, object: self.point)
            } catch {
                NotificationCenter.default.post(name: .NotificationUpdateDB, object: nil)
            }
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    func addChilds(name: String, latide: Double, longtude: Double){
         ref.child("names").updateChildValues([Date().asUUID: ["name":name, "latitude":latide,"longitude":longtude]])
    }
    
    func remove(keysChild: String) {
        ref.child("names").child(keysChild).removeValue(completionBlock: { (error, refer) in
            if error != nil {
                print(error!)
            } else {
                print(refer)
                print("Child Removed Correctly")
            }
        })
    }
    
    func removeObserver(){
        ref.removeAllObservers()
    }
}
