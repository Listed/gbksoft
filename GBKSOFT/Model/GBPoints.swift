//
//  GBPoints.swift
//  GBKSOFT
//
//  Created by admin on 6/13/19.
//  Copyright © 2019 Andrew Sasin. All rights reserved.
//

import Foundation

struct GBPoints {
    var names:[String:coordinate]?
}

struct coordinate:Codable {
    var name: String?
    var latitude: Double?
    var longitude: Double?
}

extension GBPoints: Codable {
    init(dictionary: [String: Any]) throws {
        self = try JSONDecoder().decode(GBPoints.self, from: JSONSerialization.data(withJSONObject: dictionary))
    }
}
